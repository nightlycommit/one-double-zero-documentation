---
layout: default
title: Home
nav_order: 1
---
# Welcome to One Double Zero Documentation

This is the documentation for One Double Zero, the JavaScript code coverage tool aiming at accuracy, simplicity, flexibility and speed.

## Prerequisites

* The command-line utility `odz` requires Node.js 18 or more.

## Getting started

The recommended way to install One Double Zero is to install it globally from its [npm package](https://www.npmjs.com/package/one-double-zero).

```shell
$ npm i one-double-zero -g
```

If all went well, you should be able to open a command prompt, and execute `odz version`.

```shell
$ odz version
1.2.3
```

Once installed, the most basic way to use One Double Zero is to execute its included CLI `odz`, passing it the command that needs to be executed.

```shell
odz npm run test
```

Doing such makes `odz` execute the command `npm run test`, and then compute the coverage for the source files that match the default glob pattern `**/*.{js,mjs,jsx,ts,mts,tsx}`.

The glob patterns used by `odz` to identify the source files are controlled by the `sources` flag.

```shell
odz --sources=src/**/*.mts npm run test
```

In order to not have to pass the `sources` flag on every execution of `odz`, it is recommended to create a configuration file to hold the reusable configuration of `odz`. This configuration file can be provided as a JSON file named `.odzrc.json`, or a TOML file named `.odzrc.toml`.

* `.odzrc.json`

```json
{
  "sources": [
    "src/**/*.mts" 
  ]
}
```

* `.odzrc.toml`

```toml
sources = [
   "src/**/*.mts" 
]
```

## Going further

One Double Zero is powerful and flexible, and even comes with a [JavaScript API]. Please read the rest of this documentation to get to know it, starting from the [Command line usage] section.

[Command line usage]: {{ site.baseurl }}{% link command-line-usage.md %}
[JavaScript API]: {{ site.baseurl }}{% link javascript-api.md %}