/**
 * @param element {ParentNode}
 * @return {number | undefined}
 */
const getPositionInParent = (element) => {
    const parentNode = element.parentNode;

    for (let index = 0; index < parentNode.children.length; index++) {
        if (parentNode.children[index] === element) {
            return index;
        }
    }
};

/**
 * @param element {HTMLElement | ParentNode}
 */
const removeActiveClass = (element) => {
    const listItems = element.children;

    [...listItems].forEach((listItem) => {
        listItem.classList.remove("active")
    });
};

/**
 * handleTabClick
 * @param button {HTMLButtonElement}
 */
const displayTab = (button) => {
    const tab = button.parentNode;
    const tabHeader = tab.parentNode;
    
    if (tab.className.includes("active")) {
        return;
    }
    
    const groupName = tabHeader.getAttribute("data-tab");
    
    if (!groupName) {
        return;
    }

    const tabContent = document.getElementById(groupName);
    
    removeActiveClass(tabHeader);
    removeActiveClass(tabContent);

    const positionInParent = getPositionInParent(tab);
    const content = tabContent.children[positionInParent];
    
    content.classList.add("active");
    tab.classList.add("active");
};

document.addEventListener("DOMContentLoaded", () => {
    /**
     * @type {NodeListOf<HTMLButtonElement>}
     */
    const buttons = document.querySelectorAll("ul.tab > li > a");
    
    buttons.forEach((button) => {
        button.addEventListener("click", (event) => {
            event.preventDefault();
            
            displayTab(button);
        });
    });
});