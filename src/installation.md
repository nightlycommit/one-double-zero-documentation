---
layout: default
title: Installation
nav_order: 2
---
# Installation

The recommended way to install One Double Zero is to install it globally from its [npm package](https://www.npmjs.com/package/one-double-zero).

```shell
$ npm i one-double-zero -g
```

## Checking the installation

If all went well, you should be able to open a command prompt, and execute `odz version`.

```shell
$ odz version
1.2.3
```