A flag that controls whether the [run] command should append the coverage data to the coverage directory instead of overwriting its content.

Defaults to `false`.

{% tabs method %}

{% tab method Command line %}
The `append` flag can be enabled by passing either the `-a` or the `--append` command line flag to `odz`.

```shell
$ odz run -a
$ odz run --append
```
{% endtab %}

{% tab method Configuration file %}
The `append` flag can be enabled by setting the `append` property in the configuration file to `true`.

{% tabs format %}

{% tab format JSON %}
```json
{
  "append": true
}
```
{% endtab %}

{% tab format TOML %}
```toml
append = true
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

[run]: {% link command-line-usage.md %}#run