---
layout: default
title: Contributing
nav_order: 6
---
# Contributing

Both One Double Zero and this documentation source codes are open source. You are strongly encouraged to open issues and discussions on their respective repositories and to contribute to their future.

* One Double Zero: [https://gitlab.com/nightlycommit/one-double-zero](https://gitlab.com/nightlycommit/one-double-zero){:target="_blank"}
* Documentation: [https://gitlab.com/nightlycommit/one-double-zero-documentation](https://gitlab.com/nightlycommit/one-double-zero-documentation){:target="_blank"}