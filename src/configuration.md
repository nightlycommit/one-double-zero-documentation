---
layout: default
title: Configuration
nav_order: 4
---
# Configuration

## Syntax

One Double Zero configuration can be specified through command line flags, and in a configuration file.

Command line arguments have higher priority than the configuration file.

### Configuration file

One Double Zero configuration can be specified in a configuration file. This makes it easier to re-run `odz` with consistent settings, and also allows for specification of options that are otherwise only available in the JavaScript API.

One Double Zero searches for a configuration file named `.odzrc.json` or `.odzrc.toml`, in that order, starting from
the current working directory and walking up the filesystem tree.

## Configuration reference

### append

A flag that controls whether the [run] command should append the coverage data to the coverage directory instead of overwriting its content.

Defaults to `false`.

{% tabs method %}

{% tab method Command line %}
The `append` flag can be enabled by passing either the `-a` or the `--append` command line flag to `odz`.

```shell
$ odz run -a
$ odz run --append
```
{% endtab %}

{% tab method Configuration file %}
The `append` flag can be enabled by setting the `append` property in the configuration file to `true`.

{% tabs format %}

{% tab format JSON %}
```json
{
  "append": true
}
```
{% endtab %}

{% tab format TOML %}
```toml
append = true
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### branchesThreshold

A percentage that controls the branches threshold the [check] command should compare the branches coverage to.

Defaults to `100`.

{% tabs method %}

{% tab method Command line %}
The `branchesThreshold` value can be configured through either the `-bt` or the `--branches-threshold` command line flags.

```shell
$ odz check -bt 80
$ odz check --branches-threshold=80
```
{% endtab %}

{% tab method Configuration file %}
The `branchesThreshold` value can be configured through the `thresholds.branches` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "thresholds": {
    "branches": 80
  }
}
```
{% endtab %}

{% tab format TOML %}
```toml
[thresholds]
branches = 80
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### coverageDirectory

A string that controls the path of the directory where the [run] command writes the coverage data to, and where the [check] and [report] commands read the coverage data from.

{% tabs method %}

{% tab method Command line %}
The `coverageDirectory` value can be configured through either the `-cd` or the `--coverage-directory` command line flags.

```shell
$ odz run -cd .coverage
$ odz run --coverage-directory=.coverage
```
{% endtab %}

{% tab method Configuration file %}
The `coverageDirectory` value can be configured through the `coverageDirectory` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "coverageDirectory": ".coverage"
}
```
{% endtab %}

{% tab format TOML %}
```toml
coverageDirectory = ".coverage"
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}


### excludedSources

An array of glob patterns used by the [check], [report] and [run] commands to resolve the files to exclude from the files to compute coverage for.

Defaults to `[]`.

{% tabs method %}

{% tab method Command line %}
The `excludedSources` value can be configured through either the `-S` or the `--excluded-sources` command line flags.

```shell
$ odz report -S src/**/*.test.ts -S src/**/*.fixture.ts
$ odz report --excluded-sources src/**/*.test.ts --excluded-sources src/**/*.fixture.ts
```
{% endtab %}

{% tab method Configuration file %}
The `excludedSources` value can be configured through the `excludedSources` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "excludedSources": [
    "src/**/*.test.ts",
    "src/**/*.fixture.ts"
  ]
}
```
{% endtab %}

{% tab format TOML %}
```toml
excludedSources = [ "src/**/*.test.ts", "src/**/*.fixture.ts" ]
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}


### functionsThreshold

A percentage that controls the functions threshold the [check] command should compare the functions coverage to.

Defaults to `100`.

{% tabs method %}

{% tab method Command line %}
The `functionsThreshold` value can be configured through either the `-ft` or the `--functions-threshold` command line flags.

```shell
$ odz check -ft 80
$ odz check --functions-threshold=80
```
{% endtab %}

{% tab method Configuration file %}
The `functionsThreshold` value can be configured through the `thresholds.functions` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "thresholds": {
    "functions": 80
  }
}
```
{% endtab %}

{% tab format TOML %}
```toml
[thresholds]
functions = 80
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### linesThreshold

A percentage that controls the lines threshold the [check] command should compare the lines coverage to.

Defaults to `100`.

{% tabs method %}

{% tab method Command line %}
The `linesThreshold` value can be configured through either the `-lt` or the `--lines-threshold` command line flags.

```shell
$ odz check -lt 80
$ odz check --lines-threshold=80
```
{% endtab %}

{% tab method Configuration file %}
The `linesThreshold` value can be configured through the `thresholds.lines` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "thresholds": {
    "lines": 80
  }
}
```
{% endtab %}

{% tab format TOML %}
```toml
[thresholds]
lines = 80
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### logLevel

A string that controls the level of information output by One Double Zero.

Accepts `"info"` and `"debug"`.

Defaults to `"info"`.

{% tabs method %}

{% tab method Command line %}
The `logLevel` value can be configured through either the `-ll` or the `--log-level` command line flags.

```shell
$ odz report -ll debug
$ odz report --log-level=debug
```
{% endtab %}

{% tab method Configuration file %}
The `logLevel` value can be configured through the `logLevel` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "logLevel": "debug"
}
```
{% endtab %}

{% tab format TOML %}
```toml
logLevel = "debug"
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### perFile

A flag that controls whether the [check] command should compare the coverage to the thresholds per file, or globally.

Defaults to `false`.

{% tabs method %}

{% tab method Command line %}
The `perFile` flag can be enabled by passing either the `-pf` or the `--per-file` command line flag to `odz`.

```shell
$ odz check -pf
$ odz check --per-file
```
{% endtab %}

{% tab method Configuration file %}
The `perFile` flag can be enabled by setting the `perFile` property in the configuration file to `true`.

{% tabs format %}

{% tab format JSON %}
```json
{
  "perFile": true
}
```
{% endtab %}

{% tab format TOML %}
```toml
perFile = true
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### reporters

An array of [Istanbul reporter names](https://istanbul.js.org/docs/advanced/alternative-reporters/){:target="_blank"} that controls the reporters that should be executed by the [report] and [run] commands.

Defaults to `["text"]`.

{% tabs method %}

{% tab method Command line %}
The `reporters` value can be configured through either the `-r` or the `--reporters` command line flags.

```shell
$ odz report -r text -r html
$ odz report --reporters=text --reporters=html
```
{% endtab %}

{% tab method Configuration file %}
The `reporters` value can be configured through the `reporters` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "reporters": [
    "text",
    "html"
  ]
}
```
{% endtab %}

{% tab format TOML %}
```toml
reporters = [ "text", "html" ]
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### reportsDirectory

A string that controls the path of the directory where the [report] and [run] commands output the reports to.

Defaults to `".odz_output"`.

{% tabs method %}

{% tab method Command line %}
The `reportsDirectory` value can be configured through either the `-rd` or the `--reports-directory` command line flags.

```shell
$ odz run -rd .reports
$ odz run --reports-directory=.reports
```
{% endtab %}

{% tab method Configuration file %}
The `reportsDirectory` value can be configured through the `reportsDirectory` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "reportsDirectory": ".reports"
}
```
{% endtab %}

{% tab format TOML %}
```toml
reportsDirectory = ".reports"
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### reporterOptions

A key-value map that controls the reporter options that should be passed to the reporters by the [report] and [run] commands, where each key is the name of a reporter, and each value is a record of options to pass to the said reporter.

Defaults to `{}`.

The `reporterOptions` value can be configured through the `reporterOptions` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "reporterOptions": {
    "html": {
      "defaultSummarizer": "nested"
    }
  }
}
```
{% endtab %}

{% tab format TOML %}
```toml
[reporterOptions.html]
defaultSummarizer = "nested"
```
{% endtab %}

{% endtabs %}

### sources

An array of glob patterns used by the [check], [report] and [run] commands to resolve the files to compute coverage for.

Defaults to `["**/*.{js,mjs,jsx,ts,mts,tsx}"]`.

{% tabs method %}

{% tab method Command line %}
The `sources` value can be configured through either the `-s` or the `--sources` command line flags.

```shell
$ odz report -s src/main/**/*.ts -s src/cli/**/*.ts
$ odz report --sources=src/main/**/*.ts --sources=src/cli/**/*.ts
```
{% endtab %}

{% tab method Configuration file %}
The `sources` value can be configured through the `sources` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "sources": [
    "src/main/**/*.ts",
    "src/cli/**/*.ts"
  ]
}
```
{% endtab %}

{% tab format TOML %}
```toml
sources = [ "src/main/**/*.ts", "src/cli/**/*.ts" ]
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

### statementsThreshold

A percentage that controls the statements threshold the [check] command should compare the statements coverage to.

Defaults to `100`.

{% tabs method %}

{% tab method Command line %}
The `statementsThreshold` value can be configured through either the `-st` or the `--statements-threshold` command line flags.

```shell
$ odz check -st 80
$ odz check --statements-threshold=80
```
{% endtab %}

{% tab method Configuration file %}
The `statementsThreshold` value can be configured through the `thresholds.statements` property in the configuration file.

{% tabs format %}

{% tab format JSON %}
```json
{
  "thresholds": {
    "statements": 80
  }
}
```
{% endtab %}

{% tab format TOML %}
```toml
[thresholds]
statements = 80
```
{% endtab %}

{% endtabs %}

{% endtab %}

{% endtabs %}

[check]: {% link command-line-usage.md %}#check
[report]: {% link command-line-usage.md %}#report
[run]: {% link command-line-usage.md %}#run
