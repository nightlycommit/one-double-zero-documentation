---
layout: default
title: Command line usage
nav_order: 3
check_command_summary: "Check that the coverage of the configured source files is at least greater than or equal to the configured thresholds, and exit with an error if this is not the case."
report_command_summary: "Report the coverage of the configured source files, using the configured reporters."
run_command_summary: "Run the passed command, and collects coverage data along the way."
version_command_summary: "Output the version of One Double Zero to the terminal."
---
# Command line usage

Once installed, a command-line script called `odz` is placed on your path.

By default, `odz` runs the command passed as last argument, collecting coverage data along the way, and executes the configured
reporters.

It also supports the following commands:

* [check](#check) - {{ page.check_command_summary }}
* [report](#report) - {{ page.report_command_summary }}
* [run](#run) - {{ page.run_command_summary }}
* [version](#version) - {{ page.version_command_summary }}

Help is available by passing the `--help` flag to `odz`, or any of its commands:

```shell
$ odz --help
$ odz run --help
```

## Commands

### check

```shell
odz check [options]
```

{{ page.check_command_summary }}

The `check` command also outputs to the terminal the reasons why it failed.

In addition to the `-h, --help` flag, the `check` command accepts the following flags:

{% include options/branches_threshold/flags.md %}
Controls the value of the [branchesThreshold] option.

{% include options/coverage_directory/flags.md %}
Controls the value of the [coverageDirectory] option.

{% include options/functions_threshold/flags.md %}
Controls the value of the [functionsThreshold] option.

{% include options/lines_threshold/flags.md %}
Controls the value of the [linesThreshold] option.

{% include options/log_level/flags.md %}
Controls the value of the [logLevel] option.

{% include options/per_file/flags.md %}
Controls the value of the [perFile] option.

{% include options/sources/flags.md %}
Controls the value of the [sources] option.

{% include options/statements_threshold/flags.md %}
Controls the value of the [statementsThreshold] option.

### report

```shell
odz report [options]
```

{{ page.report_command_summary }}

The `report` command expects some coverage data to be present in the configured coverage directory.

The `report` command only supports [Istanbul reporters](https://istanbul.js.org/docs/advanced/alternative-reporters/). Please use the JavaScript API to execute custom reporters.

In addition to the `-h, --help` flag, the `report` command accepts the following flags:

{% include options/coverage_directory/flags.md %}
Controls the value of the [coverageDirectory] option.

{% include options/log_level/flags.md %}
Controls the value of the [logLevel] option.

{% include options/reporters/flags.md %}
Controls the value of the [reporters] option.

{% include options/reports_directory/flags.md %}
Controls the value of the [reportsDirectory] option.

{% include options/sources/flags.md %}
Controls the value of the [sources] option.

### run

```shell
odz run [options] <command>
```

{{ page.run_command_summary }}

The `run` command is technically agnostic of the passed command (i.e. it is perfectly possible to pass `ls -la` as command), but, logically, it expects the passed command to collect coverage data when the `NODE_V8_COVERAGE` environment variable is set.

Said differently, in its current form, it is opinionated towards a Node.js script, and no other way to trigger the collection of coverage data is supported by the `run` command.

In addition to the `-h, --help` flag, the `run` command accepts the following flags:

{% include options/append/flags.md %}
Controls the value of the [append] option.

{% include options/coverage_directory/flags.md %}
Controls the value of the [coverageDirectory] option.

{% include options/log_level/flags.md %}
Controls the value of the [logLevel] option.

### version

{{ page.version_command_summary }}

The `version` command only supports the `-h, --help` flag.

[Foo]: Report the coverage of the configured source files, using the configured reporters
[Configuration reference]: {{ site.baseurl }}{% link configuration.md %}
[append]: {{ site.baseurl }}{% link configuration.md %}#append
[branchesThreshold]: {{ site.baseurl }}{% link configuration.md %}#branchesthreshold
[coverageDirectory]: {{ site.baseurl }}{% link configuration.md %}#coveragedirectory
[functionsThreshold]: {{ site.baseurl }}{% link configuration.md %}#functionsthreshold
[linesThreshold]: {{ site.baseurl }}{% link configuration.md %}#linesthreshold
[logLevel]: {{ site.baseurl }}{% link configuration.md %}#loglevel
[perFile]: {{ site.baseurl }}{% link configuration.md %}#perfile
[reporters]: {{ site.baseurl }}{% link configuration.md %}#reporters
[reportsDirectory]: {{ site.baseurl }}{% link configuration.md %}#reportsdirectory
[sources]: {{ site.baseurl }}{% link configuration.md %}#sources
[statementsThreshold]: {{ site.baseurl }}{% link configuration.md %}#statementsthreshold